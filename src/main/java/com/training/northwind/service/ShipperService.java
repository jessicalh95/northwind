package com.training.northwind.service;

// Frank's code
// https://bitbucket.org/fcallaly/northwind-shippers-jpa

import com.training.northwind.entity.Shipper;
import com.training.northwind.repository.ShipperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShipperService {

    @Autowired
    ShipperRepository shipperRepository;

    // find all method
    public List<Shipper> findAll() {
        return shipperRepository.findAll();
    }

    // method to find by ID
    public Shipper findById(long id) {
        return shipperRepository.findById(id).get();
    }

    public List<Shipper> findByPhone(String phone) {
        return shipperRepository.findShipperByPhone("1234");
    }

    // method to add a new shipper
    public Shipper save(Shipper shipper) {
        return shipperRepository.save(shipper);
    }
}
