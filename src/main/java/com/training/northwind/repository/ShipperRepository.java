package com.training.northwind.repository;

// Frank's code
// https://bitbucket.org/fcallaly/northwind-shippers-jpa

import com.training.northwind.entity.Shipper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

// @Component or @Repository or @Service would also achieve the same thing
@Repository
public interface ShipperRepository extends JpaRepository<Shipper, Long> {

    List<Shipper> findShipperByPhone(String phone);

}
