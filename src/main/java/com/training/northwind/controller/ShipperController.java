package com.training.northwind.controller;

// Frank's code
// https://bitbucket.org/fcallaly/northwind-shippers-jpa

import com.training.northwind.entity.Shipper;
import com.training.northwind.service.ShipperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/v1/shipper")
public class ShipperController {

    @Autowired
    private ShipperService shipperService;

    @GetMapping
    public List<Shipper> findAll() {
        return shipperService.findAll();
    }

    @GetMapping("/{id}") // I expect there to be a number on the end of the URL - id = variable name
    public ResponseEntity<Shipper> findById(@PathVariable long id) { // path variable when it's required not optional - query parameter for optional (use @RequestParam then)
        try {
            return new ResponseEntity<Shipper>(shipperService.findById(id), HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            // return 404
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<Shipper> create(@RequestBody Shipper shipper) {
        return new ResponseEntity<Shipper>(shipperService.save(shipper), HttpStatus.CREATED);
    }

    // listen for requests coming into
    // localhost if shipper then call this
    // now if I type localhost:8080/shipper in chrome
    // pong will print on screen
//    @GetMapping
//    public String ping() {
//        return "pong";
//    }

    // in this case
    // need to enter: http://localhost:8080/shipper/ping into chrome
//    @GetMapping("ping")
//    public String ping() {
//        return "pong";
//    }

}
