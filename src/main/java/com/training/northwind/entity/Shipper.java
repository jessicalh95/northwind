package com.training.northwind.entity;

// Frank's code
// https://bitbucket.org/fcallaly/northwind-shippers-jpa

import javax.persistence.*;

@Entity(name = "shippers") // shippers is the table name
public class Shipper {

    // declaring properties
    // these match the columns of the shipper table
    @Id // telling that the id is the primary key
    @GeneratedValue(strategy = GenerationType.IDENTITY) // the equivalent of auto increment - don't get dups
    @Column(name = "ShipperID") // because id is called ShipperID in the table
    private Long id;

    @Column(name = "CompanyName") // lower case due to case sensitivity
    private String companyName;

    @Column(name = "Phone")
    private String phone;

    // getters and setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    // methods
    @Override
    public String toString() {
        return "Shipper{" +
                "id=" + id +
                ", companyName='" + companyName + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
