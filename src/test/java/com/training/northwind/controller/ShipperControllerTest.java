package com.training.northwind.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ShipperControllerTest {

    // not a unit test it's an integration test because it's testing multiple things
    // like: controller, service, repository and mysql db

    // MockMvc will let me run mock http requests and do stuff with the response
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shipperControllerFindAllSuccess() throws Exception {
        // perform method to send requests to urls - in this case a get request
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/shipper"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        // NOTE should test this on a mock service instead
        // should create a mock db within the program/ application for the duration of the test
        // this would be a portable integration test

    }

    @Test
    public void shipperControllerFindAllNotFound() throws Exception {
        // perform method to send requests to urls - in this case a get request
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/ship"))
                .andDo(print())
                .andExpect(status().isNotFound()) // is not found - so will pass
                .andReturn();

        // NOTE should test this on a mock service instead
        // should create a mock db within the program/ application for the duration of the test
        // this would be a portable integration test

    }

}
