package com.training.northwind.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShipperTest {

    private static final String testCompanyName = "JUnit Test Company";
    private static final String testPhone = "028 90866744";
    private Shipper shipper;

    @BeforeEach
    public void setup() {
        this.shipper = new Shipper();
    }

    @Test
    public void setGetCompanyNameTest() {
        this.shipper.setCompanyName(testCompanyName);
        assertEquals(this.shipper.getCompanyName(), testCompanyName);
    }

    @Test
    public void setGetPhoneTest() {
        this.shipper.setPhone(testPhone);
        assertEquals(this.shipper.getPhone(), testPhone);
    }

}

